﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProjectSigma.Model;
using ProjectSigma.ViewModels;

namespace ProjectSigma.Repository
{
    public class MessageRepository : IMessageRepository
    {
        private ApplicationDbContext _context;

        public MessageRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Message> GetAllMessages()
        {
            return _context.Messages.OrderBy(x => x.DateTime).ToList();
        }

        public void AddMessage(ContactViewModel messageVm)
        {
            var message = Mapper.Map<Message>(messageVm);
            message.DateTime = DateTime.UtcNow;
            _context.Messages.Add(message);
        }

        public bool SaveAll()
        {
            return _context.SaveChanges() > 0;
        }
    }
}
