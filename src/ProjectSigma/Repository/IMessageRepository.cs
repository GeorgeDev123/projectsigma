﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectSigma.Model;
using ProjectSigma.ViewModels;

namespace ProjectSigma.Repository
{
    public interface IMessageRepository
    {
        IEnumerable<Message> GetAllMessages();
        void AddMessage(ContactViewModel messageVm);
        bool SaveAll();
    }
}
