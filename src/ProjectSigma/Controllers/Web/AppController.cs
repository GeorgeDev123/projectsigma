﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectSigma.Repository;
using ProjectSigma.Services;
using ProjectSigma.ViewModels;

namespace ProjectSigma.Controllers.Web
{
    public class AppController : Controller
    {
        private readonly IMailService _mailService;
        private readonly IMessageRepository _repository;

        public AppController(IMailService service, IMessageRepository repository)
        {
            _mailService = service;
            _repository = repository;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Contact(ContactViewModel model)
        {
            if (ModelState.IsValid)
            {
                var email = Startup.Configuration["AppSettings:SiteEmailAddress"];
                if (string.IsNullOrWhiteSpace(email))
                {
                    ModelState.AddModelError("", "Could not send email, configuration problem");
                }
                _repository.AddMessage(model);
                if (_repository.SaveAll() && await _mailService.SendMail(model))
                {
                    ModelState.Clear();
                    ViewBag.Error = false;
                    ViewBag.Message = "Mail sent, Thanks";
                }
                else
                {
                    ViewBag.Error = true;
                    ViewBag.Message = "Sorry, there is an error pls try again later";
                }
            }

            return View();
        }
    }
}
