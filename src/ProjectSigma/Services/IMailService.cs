﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectSigma.ViewModels;

namespace ProjectSigma.Services
{
    public interface IMailService
    {
        Task<bool> SendMail(ContactViewModel vm);
    }
}
