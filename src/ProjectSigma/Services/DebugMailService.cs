﻿using System;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using Flurl.Http;
using ProjectSigma.ViewModels;

namespace ProjectSigma.Services
{
    public class DebugMailService : IMailService
    {

        public async Task<bool> SendMail(ContactViewModel vm)
        {
            try
            {
                var res = await Startup.Configuration["MailServerApi:uri"].PostUrlEncodedAsync(
                    new {
                        api_user = Startup.Configuration["MailServerApi:api_user"],
                        api_key = Startup.Configuration["MailServerApi:api_key"],
                        to = Startup.Configuration["MailServerApi:to"],
                        toname = Startup.Configuration["MailServerApi:toname"],
                        subject = Startup.Configuration["MailServerApi:subject"],
                        text = vm.Message + Environment.NewLine + "Phone number is: " + vm.Phone,
                        from = vm.Email
                    });
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }
                return false;
            }
            catch (Exception exception)
            {
                Debug.Write(exception.Message);
                return false;
            }
        }
    }
}
