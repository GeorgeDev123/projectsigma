﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProjectSigma.Model;
using Microsoft.EntityFrameworkCore;
using ProjectSigma.Repository;
using ProjectSigma.Services;
using ProjectSigma.ViewModels;

namespace ProjectSigma
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public static IConfigurationRoot Configuration { get; set; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder().SetBasePath(env.ContentRootPath).AddJsonFile("config.json");
            Configuration = builder.Build();
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplicationInsightsTelemetry(Configuration);
            services.AddDbContext<ApplicationDbContext>(
                options => options.UseSqlServer(Configuration["Data:DefaultConnection"]));
            services.AddMvc();
            services.AddTransient<IMailService, DebugMailService>();
            services.AddTransient<ApplicationDbContext, ApplicationDbContext>();
            services.AddTransient<IMessageRepository, MessageRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseStaticFiles();
            Mapper.Initialize(config =>
            {
                config.CreateMap<Message, ContactViewModel>()
                .ForMember(dest => dest.Message, opts => opts.MapFrom(src => src.Content))
                .ReverseMap().ForMember(dest => dest.Content, opt => opt.MapFrom(src => src.Message));
            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                          name: "default",
                          template: "{controller=App}/{action=Index}/{id?}");
            });
        }
    }
}
